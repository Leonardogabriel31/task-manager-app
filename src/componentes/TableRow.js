import React from 'react'
import Modal from './Modal';
import Typography from '@mui/material/Typography';
import { useModal } from './useModal';
import { RiCloseCircleLine } from 'react-icons/ri';
import { TiEdit } from 'react-icons/ti';
import { FiEye } from 'react-icons/fi';



const RowTable = ({el, data, setDataToEdit, deleteTarea}) => {
    let {name, description, id} = el;

    const [isOpenModal1, openModal1, closeModal1] = useModal(false);
    
    
    
    return(
        <div className='lista'>
            <div>
                {name}
            </div>
            <div className='icons'>
                <FiEye 
                    onClick={openModal1}
                    className='watch-icon'
                />
                 {/* MODAL */}
                <Modal isOpen={isOpenModal1} closeModal={closeModal1}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Descripcion de la tarea
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        {description}
                    </Typography>
                </Modal>
                <TiEdit
                    onClick={() => setDataToEdit(el)}
                    className='edit-icon'
                />
                <RiCloseCircleLine
                    onClick={() => deleteTarea(id)}
                    className='delete-icon'
                />
            </div>  
        </div>
    ) 
}

export default RowTable

