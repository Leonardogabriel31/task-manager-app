import React, { useEffect, useState } from 'react'
import { styled } from '@mui/material/styles';
import { Grid } from '@mui/material';
import swal from 'sweetalert';
import Formulario from './Formulario';
import TableTarea from './Table';
import Paper from '@mui/material/Paper';
import { helpHttp } from '../helpers/helpHttp';
import Loader from './Loader';
import Message from './Message';

// app crud
const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : 'transparent',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    display: 'flexbox',
    color: theme.palette.text.secondary,
  }));

const AppCrud = () => {
    const [db, setDb] = useState(null)

    const [dataToEdit, setDataToEdit] = useState(null);

    const [error, setError] = useState(null);

    const [loading, setLoading] = useState(false);

    let api = helpHttp();
    let url = "http://localhost:8080/Tasks";

    useEffect(() => {
        setLoading(true);
        helpHttp().get(url).then((res) => {
            // console.log(res);
            if (!res.err) {
                setDb(res);
                setError(null);
            }else {
                setDb(null);
                setError(res);
            }

            setLoading(false);
        });
    }, [url]);

    const createTarea = (data) => {
        data.id = Date.now();
        // console.log(data);

        let options = {
            body:data, 
            headers: {"content-type":"application/json"},
        };

        api.post(url, options).then((res) => {
            console.log(res);
            if(!res.err){
                setDb([...db, res]);
            } else {
                setError(res);
            }
        });        
        
    }

    const updateTarea = (data) => {

        swal({
            title: "Editar",
            text: "Estas seguro que deseas editar esta tarea?",
            icon: "warning",
            buttons: ["No", "Si"]
        }).then(respuesta => {
            if(respuesta){
                let endpoint = `${url}/${data.id}`;
                // console.log(endpoint);
            
                let options = {
                    body:data, 
                    headers: {"content-type":"application/json"},
                };

                api.put(endpoint, options).then((res) => {
                    // console.log(res);
                    if(!res.err){
                        let newData = db.map((el) => (el.id === data.id ? data : el));
                        setDb(newData);
                        swal({
                            text: "La tarea ha sido editada con exito",
                            icon: "success"
                        })
                    } else {
                        setError(res);
                    }
                });
            }
        })        
    }

    const deleteTarea = (id) => {
        
        swal({
            title: "Eliminar",
            text: "Estas seguro que deseas eliminar esta tarea?",
            icon: "error",
            buttons: ["No", "Si"]
        }).then(respuesta => {
            if(respuesta){
                let endpoint = `${url}/${id}`;
                let options = {
                    headers: {"content-type":"application/json"},
                };
                api.del(endpoint, options).then(res => {
                    if(!res.err){ 
                        let newData = db.filter((el) => el.id !== id);
                        setDb(newData);

                        swal({text: "La tarea ha sido eliminada con exito",
                        icon: "success"})
                        
                    } else {
                        setError(res);
                    }
                }); 
            } else {
                return;
            }
        })
    }

    
    return (
        <div className='contenedor-grid'>
            <Grid container spacing={2}>
                <Grid item xs={4}>
                    <Item>
                        <Formulario 
                            createTarea={createTarea} 
                            updateTarea={updateTarea} 
                            dataToEdit={dataToEdit}
                            setDataToEdit={setDataToEdit}
                        />
                    </Item>
                </Grid>
                <Grid item xs={8}>
                    <Item>
                        {loading && <Loader />}
                        {error && (
                            <Message 
                                msg={`Error ${error.status}: ${error.statusText}`} 
                                bgColor="red" 
                            />
                        )}
                        {db && (
                            <TableTarea 
                                data={db} 
                                setDataToEdit={setDataToEdit}
                                deleteTarea={deleteTarea}
                            />
                        )}
                    </Item>
                </Grid>
            </Grid> 
        </div> 
    )
}

export default AppCrud

