import React, {useState, useEffect} from 'react'
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import swal from 'sweetalert';

// formulario
const initialForm = {
    name: '',
    description: '',
    id: null
    }

    const CssTextField = styled(TextField)({
      //Cuando el input tiene el foco
      '& label.Mui-focused': {
        color: 'white',
      },
      //Color inicial del borde del input sin tener el foco
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: 'white',
        },
        //Color del borde al pasar el mouse por encima
        '&:hover fieldset': {
          borderColor: 'green',
        },
        //Color del borde al hacer click
        '&.Mui-focused fieldset': {
          borderColor: 'green',
        },
        //Color del texto
        '&.css-9ddj71-MuiInputBase-root-MuiOutlinedInput-root':{
          color:'white'
        },
      },
    });

const Formulario = ({createTarea, updateTarea, dataToEdit, setDataToEdit}) => {

  const [form, setForm] = useState({initialForm});

  useEffect(() => {
        if(dataToEdit){
            setForm(dataToEdit);
        }else{
            setForm(initialForm);
        }
  },[dataToEdit])

  const handleChange = (event) => {
      setForm({
          ...form,
          [event.target.name]:event.target.value,
      })
  }

  const enviarDatos = (event) => {
      event.preventDefault();
      
      if(!form.name || !form.description){
          
        swal({
          title: "Presta atencion!!",
          text: "Debes introducir los datos en el formulario",
          icon: "warning",
          button: "Aceptar"
        });

        return;
      }
      if(form.id === null){
        createTarea(form);
      }else{
          updateTarea(form);
      }
      resetFormulario(); 
  };

  const resetFormulario = (event) => {
      setForm(initialForm);
      setDataToEdit(null);
  }

  return (
    <div>
      <h3>{dataToEdit ? "Introduzca los datos que desea editar y presione CONFIRMAR":"Programa aqui tus tareas de formas sencilla"}</h3>
      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { m: 1, width: '25ch' },
        }}
        noValidate
        autoComplete="off"
        onSubmit={enviarDatos}
      >
        <div>
          <CssTextField 
            required
            label="Ingrese aqui su tarea"
            type='text'
            name='name'
            value={form.name}
            onChange={handleChange}           
          />

          <CssTextField 
            label="Ingrese la descripcion"
            multiline={true}
            maxRows={5}
            type='text' 
            name='description'
            value={form.description}
            inputProps={{ style: { color: "white" } }}
            onChange={handleChange}        
          />
        </div>
        <Button variant="contained" color="success" type='submit' value='enviar'>
          {dataToEdit ? "Confirmar":"Enviar"}
        </Button>
        <Button variant="contained" color="primary" type='reset' value='limpiar' onClick={resetFormulario}>
            Resetear
        </Button>

        </Box>
      
      
    </div>
  )
}

export default Formulario