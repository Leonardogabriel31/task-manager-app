import React from 'react'
import RowTable from './TableRow'

 
const TableTareas = ({data, setDataToEdit, deleteTarea}) => {
    return(
        <div className='lista'>
            <h3>A ver... Que tienes en mente?</h3>
            {data.length > 0 ? (
                data.map((el) => (
                    <div
                        className={el.isComplete ? 'todo-row complete' : 'todo-row'}
                        key={el.id}
                    >
                        <RowTable 
                            key={el.id} 
                            el={el}
                            setDataToEdit={setDataToEdit}
                            deleteTarea={deleteTarea}
                        />
                    </div>
                ))
            ):(
                <tr>
                    <td colSpan="3">No tienes tareas pendientes...</td>
                </tr>
            )} 
        </div>
    )   
}

export default TableTareas
